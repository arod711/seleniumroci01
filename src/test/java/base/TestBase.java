package base;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver driver;
    public static WebDriverWait wait;
    public static Properties config = new Properties();
    public static Properties OR = new Properties();
    public static FileInputStream fis;
    public static String user_dir;
    public static Logger log = Logger.getLogger("devpinoyLogger");
    private static String browser;

    @BeforeSuite
    public void setUp() {
        if(driver == null) {
            user_dir = System.getProperty("user.dir");

            try {
                fis = new FileInputStream(
                        user_dir + "//src//test//resources//properties//Config.properties");
                config.load(fis);
                log.debug("Config file loaded successfully");
                fis = new FileInputStream(
                        user_dir + "//src//test//resources//properties//OR.properties");
                OR.load(fis);
                log.debug("OR file loaded successfully");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(System.getenv("browser") != null && !System.getenv("browser").isEmpty()) {
                browser = System.getenv("browser");
            } else {
                browser = config.getProperty("browser");
            }


            if (browser.equals("firefox")) {
                driver = new FirefoxDriver();
            } else if (browser.equals("chrome")) {
                System.setProperty("webdriver.chrome.driver", user_dir +
                        "//src//test//resources//executables//chromedriver");
                driver = new ChromeDriver();
            }

            driver.get(config.getProperty("testsiteurl"));
            log.debug("Navigating to test site url");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(Integer.parseInt(config.getProperty("implicit.wait")),
                    TimeUnit.SECONDS);
            wait = new WebDriverWait(driver, 5);
        }
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException nse) {
            return false;
        }
    }

    @AfterSuite
    public void tearDown() {
        if (driver != null)
            driver.quit();
        log.debug("Execution completed");
    }

    public By getBy(String locator) {
        if(locator.contains("_CSS")) {
            return By.cssSelector(OR.getProperty(locator));
        } else if(locator.contains("_XPATH")) {
            return By.xpath(OR.getProperty(locator));
        }
        throw new NoSuchElementException("Element with locator " + locator + " was not found.");
    }

    public WebElement getElement(String locator) {
        return driver.findElement(getBy(locator));
    }

    public void select(String locator, String value) {
        WebElement dropDown = getElement(locator);
        Select select = new Select(dropDown);

        select.selectByVisibleText(value);
    }
}
