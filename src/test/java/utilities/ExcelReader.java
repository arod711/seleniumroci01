package utilities;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;

import static base.TestBase.user_dir;

public class ExcelReader {

    private Workbook workbook;
    public static final String DATA1_DATA_PROVIDER = user_dir +
            "//src//test//resources//excel//data1.xlsx";
    public static final String OPEN_ACCOUNT_DATA_PROVIDER = user_dir +
            "//src//test//resources//excel//openAccount.xlsx";

    public String[][] getRows(int nRows, int nCells, String file) throws IOException, InvalidFormatException {
        String[][] res = new String[nRows][nCells];
        workbook =  WorkbookFactory.create(new File(file));

/*        workbook.forEach(sheet -> {
            System.out.println("=> " + sheet.getSheetName());
        });*/

        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();

        for(int i = 1; i < nRows+1; i++)
            for(int j = 0; j < nCells; j++)
                res[i-1][j] = dataFormatter.formatCellValue(sheet.getRow(i).getCell(j));

        workbook.close();
        return res;
    }
}
