package tests;

import base.TestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utilities.ExcelReader;

import java.io.IOException;

import static utilities.ExcelReader.*;

public class CustomerTest extends TestBase {

    @DataProvider
    public Object[][] getData() {
        ExcelReader excelReader = new ExcelReader();

        try {
            return excelReader.getRows(1, 4, DATA1_DATA_PROVIDER);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        return new Object[1][1];
    }

    @Test(dataProvider = "getData")
    @Description("Test Description: Add customer")
    @Step("Add customer first name: {0} , second name: {1} , postal code: {2}")
    public void addCustomer(String firstName, String lastName, String postalCode, String alertText) {
        getElement("addCustBtn_CSS").click();
        getElement("inputFirstName_CSS").sendKeys(firstName);
        getElement("inputLastName_CSS").sendKeys(lastName);
        getElement("inputPostCode_CSS").sendKeys(postalCode);
        getElement("addCustomerBtn_CSS").click();

        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        Assert.assertTrue(alert.getText().contains(alertText));
        alert.accept();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
