package tests;

import base.TestBase;
import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BankManagerLoginTest extends TestBase {

    @Test
    @Description("Test Description: Login as bank manager")
    public void loginBankManager() {
        getElement("bmlBtn_CSS").click();
        Assert.assertTrue(isElementPresent(getBy("addCustBtn_CSS")));

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
