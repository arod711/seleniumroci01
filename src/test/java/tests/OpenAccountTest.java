package tests;

import base.TestBase;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utilities.ExcelReader;

import java.io.IOException;

import static utilities.ExcelReader.OPEN_ACCOUNT_DATA_PROVIDER;

public class OpenAccountTest extends TestBase {

    @DataProvider
    public Object[][] getData() {
        ExcelReader excelReader = new ExcelReader();

        try {
            return excelReader.getRows(3, 3, OPEN_ACCOUNT_DATA_PROVIDER);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        return new Object[1][1];
    }

    @Test(dataProvider = "getData")
    public void openAccountTest(String customer, String currency, String alertText) {
        getElement("openAccountBtn_CSS").click();
        select("customer_CSS", customer);
        select("currency_CSS", currency);
        getElement("processBtn_CSS").click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        alert.accept();
    }

}
